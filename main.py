# -*- coding: utf-8 -*-
# Copyright (c) [2023] [Osvarcha]
# Este programa está licenciado bajo la Licencia Pública General de GNU (GPL)
# Para obtener más detalles, consulte: https://www.gnu.org/licenses/gpl-3.0.html

# Creado por: Osvarcha
# Más información en: https://osvarcha.website

# Curso de Telegram Bot de atareado.es

import requests
import json

PROPERTIES = '.telegramkeys'

class TelegramBot():
    def __init__(self):
        self._token = None
        self._group = None
        self._channel = None
        with open(PROPERTIES, 'r') as file_reader:
            params = json.load(file_reader)
            self._token = params['token']
            self._group = params['group']
            self._channel = params['channel']

    def get_me(self):
        url = f"https://api.telegram.org/bot{self._token}/getMe"
        response = requests.get(url)
        if response.status_code == 200:
            salida = json.loads(response.text)
            return salida
        return None

    def get_updates(self):
        url = f"https://api.telegram.org/bot{self._token}/getUpdates"
        response = requests.get(url)
        if response.status_code == 200:
            salida = json.loads(response.text)
            return salida
        return None

    def send_message(self, chat_id, message):
        url = f"https://api.telegram.org/bot{self._token}/sendMessage"
        data = {"chat_id": chat_id, "text": message}
        response = requests.post(url, data=data)
        if response.status_code == 200:
            salida = json.loads(response.text)
            return salida
        error = json.loads(response.text)
        error_code = error['error_code']
        description = error['description']
        msg = f"Error: {error_code}. Description: {description}"
        raise Exception(msg)

    def send_message_to_group(self, message):
        try:
            return self.send_message(self._group, message)
        except Exception as exception:
            print(exception)
        return None

    def send_message_to_channel(self, message):
        try:
            return self.send_message(self._channel, message)
        except Exception as exception:
            print(exception)
        return None

if __name__ == "__main__":
    tb = TelegramBot()
    # print(tb.get_me())
    # print(tb.get_updates())
    print(tb.send_message_to_channel("demo"))
